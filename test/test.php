<?php
require_once __DIR__ . '/../vendor/autoload.php';

use QingYa\Yii2RpcClient\RpcClient;

$rpcServerUrl = '';//指定服务url
$rpcServerIp  = '';//指定服务ip 优先级高于url  格式：192.11.11.1
$result       = RpcClient::client('default', 'test', 'test')
                         ->project('rpc-center', $rpcServerUrl, $rpcServerIp)
                         ->service('Center')
                         ->serviceVersion('')//服务中心的请求，不区分版本
                         ->setIsSelect(true)
                         ->setCacheTime(100)
                         ->method('getProjectInfoForClient')
                         ->params('rpc-center', 00000, 9999)
                         ->request();
print_r($result);