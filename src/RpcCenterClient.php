<?php

namespace QingYa\Yii2RpcClient;


use Hprose\Future;
use QingYa\Helper\EnvUtils;

/**
 * Class RpcCenterClient
 * @package common\services\rpcService\rpcClient
 */
class RpcCenterClient
{

    public static function getClient($appKey = '', $appSecret = '', $from = 'client')
    {
        return RpcClient::client("rpc-center-client-from-{$from}", $appKey, $appSecret);
    }

    /**
     * 统一获取指定标识的客服端
     * @param string $appKey
     * @param string $appSecret
     * @return RpcClient
     */
    public static function getClientFromClient($appKey = '', $appSecret = '')
    {
        return self::getClient($appKey, $appSecret, 'client');
    }

    public static function getClientFromServer($appKey = '', $appSecret = '')
    {
        return self::getClient($appKey, $appSecret, 'server');
    }

    /**
     * 请求服务中心
     * 获取项目信息客户端使用，主要获取接口地址，项目随机盐
     * @param        $project   代码项目分类标识
     * @param        $secret
     * @param string $appKey    appKey
     * @param int    $hopsId    机构ID，走请求头header传递，看是否需要，也可以当做saas平台的站长ID使用
     * @param int    $projectId 机构项目ID，这两个参数用于后期，rpc服务，验证机构和项目权限使用，目前不用考虑
     * @return array|Future|mixed
     */
    public static function getProjectInfoForClient($project, $secret, $appKey = '', $hopsId = 0, $projectId = 0)
    {
        return self::getClientFromClient()
                   ->project('rpc-center')
                   ->hospId($hopsId)
                   ->projectId($projectId)
                   ->setCachetime(EnvUtils::get('rpcServer.centerCacheTime', 300))
                   ->service('Center')
                   ->method('getProjectInfoForClient')
                   ->params($project, $secret, $appKey)
                   ->request();
    }

    /**
     * 服务端调用获取项目信息 验证授权，主要根据客户端传递过来的salt判断
     * @param string $project 当前客户端所在项目
     * @param string $salt    项目随机盐 通过客户端传递过来
     * @param string $appKey
     * @return array|Future|mixed
     */
    public static function getProjectInfoForServer($project, $salt, $appKey)
    {
        $i        = 0;
        $response = [
            'code' => 10010,
            'msg'  => 'getProjectInfoForServer failed',
        ];
        while ($i <= 2) {
            $response = self::getClientFromServer()
                            ->project('rpc-center')
                            ->setCachetime(EnvUtils::get('rpcServer.centerCacheTime', 300))
                            ->service('Center')
                            ->method('getProjectInfoForServer')
                            ->params($project, $salt, $appKey)
                            ->request();
            $i++;
            // writeLog("第{$i}次请求", 'server');
            // writeLog($response, 'server');
            if ($response['code'] === 200) {
                break;
            } else if (stripos($response['msg'], '400 Bad Request') !== false) {
                // 400 强制重试
                continue;
            } else {
                break;
            }
        }
        return $response;
    }
}