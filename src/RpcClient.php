<?php

namespace QingYa\Yii2RpcClient;


use QingYa\Helper\SuperCacheHelper;
use QingYa\Yii2RpcClient\filter\FilterGzip;
use QingYa\Yii2RpcClient\filter\FilterSign;
use Exception;
use Hprose\Http\Client;
use QingYa\Helper\EnvUtils;

/**
 * Class RpcClient
 *
 * @package common\services\rpcService\rpcClient
 */
class RpcClient
{
    /**
     * RPC服务中心地址，使用配置重写
     */
    const RPC_CENTER_API_URL = 'http://yii-rpc.phpwk.com/';//可以使用配置，也可以直接固定线上rpc服务中心
    /**
     * 客户端版本
     */
    const VERSION_CLIENT = 'v1.0.0';
    /**
     * @var Client rpc客户端
     */
    protected $rpcClient;
    /**
     * @var array rpc客户端配置信息
     */
    protected $clientOptions = [
        'timeout'    => 10,//超时时间秒
        'retry'      => 2,//重试次数
        'idempotent' => true,//幂等
        'headers'    => [
        ],
    ];
    /**
     * @var string rpc项目
     */
    protected $project = '';
    /**
     * @var string rpc服务（服务类），统一走：类->方法
     */
    protected $service = '';
    /**
     * @var string rpc服务下面的具体远程方法，服务方法
     */
    protected $method = '';
    /**
     * @var string 服务版本 默认空，可以配置v1、v2等版本
     */
    protected $serviceVersion = '';


    /**
     * @var array 请求参数
     */
    protected $params = [];

    /**
     * @var array 缓存key生成参数
     */
    protected $cacheKeyParams = [];

    /**
     * @var bool 幂等性调用，(仅当前请求有效，如果需要修改当前客户端默认参数，请使用 setClientIdempotent 方法设置)，true=允许网络失败情况下重试，如果设置false的话，重试参数retry无效
     */
    protected $idempotent = null;
    /**
     * @var int 网络失败后重试次数, (仅当前请求有效，如果需要修改当前客户端默认参数，请使用 setClientRetry 方法设置)（需要先开启 幂等性调用,出现网络错误时候重试 比如500,502、404、403，程序抛异常不会重试）
     */
    protected $retry = null;
    /**
     * @var int 超时时间 (仅当前请求有效，如果需要修改当前客户端默认参数，请使用 setClientTimeout 方法设置) 单位秒
     */
    protected $timeout = null;
    /**
     * @var bool 调试签名 默认false ，设置true后客户端会记录每次的签名日志，同时如果签名错误服务端也会记录签名日志
     */
    protected $signDebug = false;

    /**
     * @var bool 是否是查询类型 默认false，如果是true的话 将允许相同签名重复请求，如果是插入或者更新操作不能设置true
     */
    protected $isSelect = false;

    /**
     * @var string rpc服务器地址
     */
    protected $rpcServerUrl = '';

    /**
     * @var string 自定义rpc服务器地址
     */
    protected $rpcServerUrlCustom = '';

    /**
     * @var string rpc服务器ip
     */
    protected $rpcServerIp = '';

    /**
     * @var int 客户端缓存时间（单位秒） 默认 0 禁用缓存
     */
    protected $cacheTime = 0;
    /**
     * @var string 缓存key，留空的话根据参数自动生成
     */
    protected $cacheKey = '';

    protected $appKey    = '';//请求方 appKey
    protected $appSecret = '';//请求方 appSecret
    protected $hospId    = 0;//机构ID
    protected $projectId = 0;//机构项目ID

    /**
     * 是否使用缓存
     *
     * @var bool 默认false=不使用，true=使用缓存
     */
    protected $usecache = false;

    /**
     * usecache = true时有效
     *
     * @var bool 默认false 标识不限制，允许缓存，true=强制不缓存，会通过header传递到服务端程序里面也做不缓存逻辑处理
     */
    protected $nocache = false;

    protected static $instance;


    public function __construct($appKey = '', $appSecret = '')
    {
        //TODO 客户端请求，默认获取当前项目的系统标识，区分请求来源，也可以通过ENV配置
        !defined('SYSTEM') && define('SYSTEM', EnvUtils::get('rpcClient.system', 'default'));

        //appKey赋值，如果项目做的有敏感配置加密，做解密处理
        $this->appKey = $appKey ?: EnvUtils::get('rpcClient.appKey');
        if (!$this->appKey) {
            //TODO---appKey做为rpc请求身份的识别，如果没有拿到，尝试其他配置获取
            $this->appKey = '';
        }
        $this->appSecret = $appSecret ?: EnvUtils::get('rpcClient.appSecret');
        //获取是否使用缓存配置
        $this->usecache  = EnvUtils::get('rpcClient.usecache', false);
        $this->rpcClient = new Client(null, false);//异步处理
        $this->rpcClient->addFilter(new FilterSign($this->appKey, md5($this->appSecret)));

        //暂时用不到
        $this->rpcClient->addFilter(new FilterGzip());
    }

    /**
     * @return Client
     */
    public function getRpcClient()
    {
        return $this->rpcClient;
    }


    /**
     * 创建rpc客户端(建议不传递，通过env配置，手动测试某一个appkey时候可以指定传递) 传递后优先高于env
     *
     * @param string $clientName
     * @param string $appKey
     * @param string $appSecret
     * @return RpcClient
     */
    public static function client($clientName = 'default', $appKey = '', $appSecret = '')
    {
        $clientName = $clientName ?: 'default';
        if (is_null(self::$instance) || (is_array(self::$instance) && !array_key_exists($clientName, self::$instance))) {
            self::$instance[$clientName] = new static($appKey, $appSecret);
        }
        return self::$instance[$clientName];
    }

    /**
     * 配置客户端全局默认参数
     *
     * @param array $clientOptions
     * @return RpcClient
     */
    public function setClientOptions(array $clientOptions)
    {
        if (isset($clientOptions['timeout'])) {
            $this->setClientTimeout($clientOptions['timeout']);
        }
        if (isset($clientOptions['retry'])) {
            $this->setClientRetry($clientOptions['retry']);
        }
        if (isset($clientOptions['idempotent'])) {
            $this->setClientIdempotent($clientOptions['idempotent']);
        }
        if (isset($clientOptions['headers'])) {
            $this->setClientHeaders($clientOptions['headers']);
        }
        return $this;
    }

    /**
     * 设置客户端超时时间 当前clientName有效
     *
     * @param int $timeout
     * @return RpcClient
     */
    public function setClientTimeout($timeout = 10)
    {
        $this->clientOptions['timeout'] = $timeout;
        return $this;
    }

    /**
     * 设置客户端失败重试次数 当前clientName有效（）
     *
     * @param int $retry
     * @return RpcClient
     */
    public function setClientRetry($retry = 2)
    {
        $this->clientOptions['retry'] = $retry;
        return $this;
    }

    /**
     * 设置客户端幂等性调用，默认true，true=允许网络失败情况下重试，如果设置false的话，重试参数retry无效
     *
     * @param bool $idempotent
     * @return RpcClient
     */
    public function setClientIdempotent($idempotent = true)
    {
        $this->clientOptions['idempotent'] = $idempotent;
        return $this;
    }

    /**
     * 设置客户端header（批量设置多个）
     *
     * @param array $headers
     * @return RpcClient
     */
    public function setClientHeaders($headers)
    {
        $this->clientOptions['headers'] = is_array($this->clientOptions['headers']) ? array_merge($this->clientOptions['headers'], $headers) : $headers;
        return $this;
    }

    /**
     * 设置客户端header（单个设置）
     *
     * @param $name
     * @param $value
     * @return RpcClient
     */
    public function setClientHeader($name, $value)
    {
        $header        = [];
        $header[$name] = $value;

        $this->clientOptions['headers'] = is_array($this->clientOptions['headers']) ? array_merge($this->clientOptions['headers'], $header) : $header;
        return $this;
    }


    /**
     * 机构ID
     *
     * @param int $hospId
     * @return RpcClient
     */
    public function hospId($hospId = 0)
    {
        $this->hospId = $hospId;
        return $this;
    }

    /**
     * 项目ID
     *
     * @param int $projectId
     * @return RpcClient
     */
    public function projectId($projectId = 0)
    {
        $this->projectId = $projectId;
        return $this;
    }

    /**
     * 是否幂等
     *
     * @return bool
     */
    public function isIdempotent()
    {
        return $this->idempotent ?? $this->clientOptions['idempotent'];
    }

    /**
     * @param bool $idempotent
     * @return RpcClient
     */
    public function setIdempotent($idempotent)
    {
        $this->idempotent = $idempotent;
        return $this;
    }

    /**
     * 获取重试次数
     *
     * @return int
     */
    public function getRetry()
    {
        return $this->retry ?? $this->clientOptions['retry'];
    }

    /**
     * @param int $retry
     * @return RpcClient
     */
    public function setRetry($retry)
    {
        $this->retry = $retry;
        return $this;
    }

    /**
     * 客服端请求，超时时间
     *
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout ?? $this->clientOptions['timeout'];
    }

    /**
     * @param int $timeout
     * @return RpcClient
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSignDebug()
    {
        return $this->signDebug;
    }

    /**
     * @param bool $signDebug
     * @return RpcClient
     */
    public function setSignDebug($signDebug = false)
    {
        $this->signDebug = $signDebug;
        return $this;
    }

    /**
     * 设置缓存时间（秒）
     *
     * @param int $cacheTime 缓存时间（秒） 0表示关闭缓存
     * @return RpcClient
     */
    public function setCacheTime($cacheTime = 0)
    {
        $this->cacheTime = $cacheTime;
        return $this;
    }

    /**
     * 设置是否是查询类型，查询类型的请求可以多次重试，不会被限制
     *
     * @param bool $isSelect
     * @return RpcClient
     */
    public function setIsSelect($isSelect = false)
    {
        $this->isSelect = $isSelect;
        return $this;
    }

    /**
     * 设置缓存key
     *
     * @param string $cacheKey 不设置的话默认自动根据所有参数hash，一般不建议设置，主要用于自定义缓存键
     * @return RpcClient
     */
    public function setCacheKey(string $cacheKey)
    {
        $this->cacheKey = $cacheKey;
        return $this;
    }

    /**
     * 从数组参数设置缓存key
     *
     * @param mixed ...$params
     * @return $this
     */
    public function setCacheKeyFromArray(...$params)
    {
        $this->cacheKeyParams = $params;
        return $this;
    }

    /**
     * rpc全局是否启用缓存
     *
     * @param bool $useCache 默认false=不走缓存，构造时通过env配置获取，也可以使用时链式配置
     * @return RpcClient
     */
    public function setUsecache($useCache)
    {
        $this->usecache = $useCache;
        return $this;
    }

    /**
     * @param bool $nocache 默认false=允许缓存，true=强制无缓存，一般不建议设置
     * @return RpcClient
     */
    public function setNocache($nocache)
    {
        $this->nocache = $nocache;
        return $this;
    }

    /**
     * 要请求的项目（非EDC项目，是代码拆分的不同项目）配置，
     *
     * @param        $project
     * @param string $rpcServerUrl
     * @param string $rpcServerIp
     * @return $this
     */
    public function project($project, $rpcServerUrl = '', $rpcServerIp = '')
    {
        $this->project = $project;
        //如果指定了rpc服务地址，优先使用
        if (!empty($rpcServerUrl)) {
            $this->rpcServerUrlCustom = $this->rpcServerUrl = $rpcServerUrl;
        }
        //如果指定rpc服务ip，最优先
        if (!empty($rpcServerIp)) {
            $this->setRpcServerIp($rpcServerIp);
        }
        return $this;
    }

    /**
     * 指定rpc服务ip
     *
     * @param $rpcServerIp
     * @return $this
     */
    public function setRpcServerIp($rpcServerIp)
    {
        $this->rpcServerIp = $rpcServerIp;
        return $this;
    }

    /**
     * 指定服务
     *
     * @param $service
     * @return $this
     */
    public function service($service)
    {
        $this->service = $service;
        return $this;
    }

    /**
     * 指定服务方法
     *
     * @param $method
     * @return $this
     */
    public function method($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * 服务版本
     *
     * @param string $serviceVersion
     * @return $this
     */
    public function serviceVersion($serviceVersion = 'v1')
    {
        $this->serviceVersion = $serviceVersion;
        return $this;
    }

    /**
     * 参数传递
     *
     * @param ...$params
     * @return $this
     */
    public function params(...$params)
    {
        $this->params = $params;
        return $this;
    }

    /**
     * 和->params方法有区别，paramsArray这个传递的一个数组，params这个传递的可变参数
     *
     * @param $paramsArray
     * @return $this
     */
    public function paramsArray($paramsArray)
    {
        if (is_array($paramsArray)) {
            $params = array_values($paramsArray);
        } else {
            $params = [];
        }
        $this->params = $params;
        return $this;
    }

    /**
     * 组合发起请求
     *
     * @return array|\Hprose\Future|mixed
     */
    public function request()
    {
        if (!$this->project) {
            return $this->error('服务项目标识不能为空');
        }
        if (empty($this->appKey) || empty($this->appSecret)) {
            return $this->error('appkey或者appsecret不能为空');
        }
        // 实际项目地址（实体项目跟项目地址一致，如果是虚拟项目的话对应的话解析后的实体项目）
        $realProject = '';
        if ($this->project == 'rpc-center') {
            $this->rpcServerUrl = self::RPC_CENTER_API_URL;
            $realProject        = 'rpc-center';
        }
        $salt = '';
        //普通的rpc服务请求，先经过rpc服务中心处理
        if ($realProject != 'rpc-center') {
            RpcCenterClient::getClientFromClient($this->appKey, $this->appSecret);
            $projectInfo = RpcCenterClient::getProjectInfoForClient($this->project, $this->appSecret, $this->appKey, $this->hospId, $this->projectId);
            //兼容项目成功返回统一使用200
            if ($projectInfo['code'] !== 200) {
                return $projectInfo;
            }
            //请求服务中心返回参数处理：主要的（serverUrl、salt）
            $rpcServerUrlFromServer = $projectInfo['data']['rpc_server_url'];//通过rpc服务中心拿到的，请求目标服务的 rpc服务地址url
            $salt                   = $projectInfo['data']['salt'];
            $realProject            = $projectInfo['data']['project'] ?? $this->project;
            // 优先级：指定地址（需要每次调用指定） > env > 服务端返回地址、env配置多个rpc服务的url地址
            $this->rpcServerUrl = $this->rpcServerUrlCustom ?: EnvUtils::get('rpcServer.' . $this->project, $rpcServerUrlFromServer);
        } else {
            // rpc-center 的情况 优先 env
            $this->rpcServerUrl = EnvUtils::get('rpcServer.center', $this->rpcServerUrl);
        }
        if (empty($this->rpcServerUrl)) {
            return $this->error("项目({$this->project})RPC服务地址不存在");
        }
        if ($this->rpcServerIp) {
            // 指定rpc服务器ip
            $rpcServerUrlArray  = parse_url($this->rpcServerUrl);
            $this->rpcServerUrl = "{$rpcServerUrlArray['scheme']}://{$this->rpcServerIp}{$rpcServerUrlArray['path']}";
            $this->rpcClient->setHeader('host', $rpcServerUrlArray['host']);
        }
        if (!$this->service) {
            return $this->error('service不能为空');
        }
        if (!$this->method) {
            return $this->error('method不能为空');
        }
        // rpc通过url实现分版本请求-yii不用，使用header参数 serviceversion = 目标服务版本
        // $serviceFull = $this->serviceVersion ? $this->serviceVersion . '/' . $this->service : $this->service;
        $this->rpcClient->useService($this->rpcServerUrl . 'rpc/' . $this->service);
        // writeLog($this->rpcServerUrl . 'rpc/' . $serviceFull, 'rpc-log');
        $starttime = microtime(true);
        try {
            // 如果缓存key未设置的话自动生成
            $this->genCacheKey();

            // 调试模式
            if ($realProject != 'rpc-center' && $this->isSignDebug()) {
                $this->nocache    = true;
                $this->idempotent = false;
                // 增加rpc日志类型，有的项目框架（fastadmin），配置的有日志记录过滤类型，根据实际项目需要调整
            }
            // 是否是缓存数据 true=缓存数据，false=非缓存数据
            $fromCache  = true;
            $requestFun = function (SuperCacheHelper $superCache) use ($salt, $realProject, &$fromCache) {
                // 走到这里的话说明不是缓存数据
                $fromCache = false;
                $this->rpcClient->setIdempotent($this->isIdempotent());
                $this->rpcClient->setRetry($this->getRetry());
                $this->rpcClient->setTimeout($this->getTimeout() * 1000);
                $this->rpcClient->useData['signdebug'] = $this->signDebug;
                $this->rpcClient->setHeader('nocache', $this->nocache);
                $this->rpcClient->setHeader('isselect', $this->isSelect);
                $this->rpcClient->setHeader('hospid', $this->hospId);
                $this->rpcClient->setHeader('projectid', $this->projectId);
                $this->rpcClient->setHeader('salt', $salt);
                $this->rpcClient->setHeader('system', SYSTEM);//项目系统标识，不同的项目自定义不同
                $this->rpcClient->setHeader('method', $this->method);
                $this->rpcClient->setHeader('realproject', $realProject);
                $this->rpcClient->setHeader('rpcclient', self::VERSION_CLIENT);
                $this->rpcClient->setHeader('serviceversion', $this->serviceVersion);
                // 自定义header
                if ($this->clientOptions['headers'] && is_array($this->clientOptions['headers'])) {
                    foreach ($this->clientOptions['headers'] as $headerName => $headerValue) {
                        $this->rpcClient->setHeader($headerName, $headerValue);
                    }
                }
                // writeLog('invoke', 'rpc-log');
                $response = $this->rpcClient->invoke($this->method, $this->params);
                // writeLog($response, 'rpc-response');
                if (!isset($response['code'])) {
                    $response = $this->error('服务端响应数据格式异常', 100006, $response);
                    $superCache->setData($response)->setNeedCache(false);
                } else {
                    $superCache->setData($response)->setNeedCache(false);
                    if (isset($response['code']) && $response['code'] === 200 && $this->cacheTime && $this->cacheKey) {
                        // 符合条件 需要缓存数据
                        $superCache->setNeedCache(true);
                    }
                }
            };
            $response   = super_cache($this->cacheKey, $requestFun, $this->cacheTime, $this->nocache, $this->usecache);

            // writeLog('response - end', 'rpc-log');
            if ($fromCache) {
                // print_r($response);
                // exit();
                $response['from']              = 'rpc client cache';
                $response['used_time_server']  = "0";
                $response['used_time_request'] = "0";
            }
        } catch (Exception $e) {
            $message  = str_replace("Wrong Response:", '', $e->getMessage());
            $response = json_decode($message, true);
            $response = $response ?: $this->error($e->getMessage());
        }
        // 总耗时
        $response['used_time_server'] = $response['used_time_server'] ?? 0;
        $response['used_time_total']  = $response['used_time_server'] ? number_format(microtime(true) - $starttime, 6) : 0;
        // 请求过程耗时
        $response['used_time_request'] = $response['used_time_total'] - $response['used_time_server'];
        if ($realProject != 'rpc-center' && $this->isSignDebug()) {
            //调试参数、也可以添加日志记录
        }
        // 请求完毕后清理部分参数
        $this->clearArgs();
        return $response;
    }

    /**
     * 执行后清理部分参数，后续增加其他参数时，注意，是否需要清空
     */
    protected function clearArgs()
    {
        // 执行的方法参数 置空
        $this->method = '';
        $this->params = [];
        // 缓存参数 置空
        $this->cacheTime = 0;
        $this->cacheKey  = '';
        // 清空本地请求参数,之后采用全局参数
        $this->timeout            = null;
        $this->retry              = null;
        $this->idempotent         = null;
        $this->isSelect           = false;
        $this->rpcServerIp        = '';
        $this->rpcServerUrlCustom = '';
        // 重置版本
        $this->serviceVersion = '';
    }

    protected function genCacheKey()
    {
        if (!$this->cacheKey) {
            $options = $this->cacheKeyParams ?: $this->params;
            $lastKey = $options ? serialize($options) : '';
            $headers = $this->clientOptions['headers'];
            if ($headers && is_array($headers)) {
                $lastKey .= serialize($headers);
            }
            $lastKey        .= $this->appKey;
            $lastKey        = md5($lastKey);
            $this->cacheKey = "{$this->project}_{$this->hospId}_{$this->projectId}:{$this->serviceVersion}_{$this->service}:{$this->method}:{$lastKey}";
        }
        return $this->cacheKey;
    }

    /**
     * 统一错误返回
     *
     * @param string $msg
     * @param int    $code 200不能占用，200为成功
     * @param array  $data
     * @return array
     */
    protected function error($msg = '', $code = 1001, $data = [])
    {
        return [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data,
        ];
    }

    /**
     * 统一成功返回
     *
     * @param string $msg
     * @param array  $data
     * @return array
     */
    protected function success($msg = '', $data = [])
    {
        return [
            'code' => 200,
            'msg'  => $msg,
            'data' => $data,
        ];
    }
}

