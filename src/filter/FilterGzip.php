<?php


namespace QingYa\Yii2RpcClient\filter;

use Hprose\Filter;
use stdClass;

/**
 * gzip数据压缩过滤器、添加自定义过滤器
 * Class FilterGzip
 * @package common\services\rpcService\rpcClient\filter
 */
class FilterGzip implements Filter
{


    public function inputFilter($data, stdClass $context)
    {
        //        $first = substr($data, 0, 1);
        //        if (in_array($first, ['{']) || preg_match('@^[\w\[\]\{\}\@]+$@iUs', substr($data, -3)) || stripos($data, 'code') !== false) {
        //            return $data;
        //        } else {
        //            return gzdecode($data);
        //        }
        return $data;
    }

    public function outputFilter($data, stdClass $context)
    {
        //        try {
        //            return gzencode($data);
        //        } catch (Exception $e) {
        //            return $data;
        //        }
        return $data;
    }

}