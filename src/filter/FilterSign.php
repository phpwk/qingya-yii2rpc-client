<?php


namespace QingYa\Yii2RpcClient\filter;


use QingYa\Yii2RpcClient\RpcClient;
use Hprose\Filter;
use Hprose\Http\Client;
use stdClass;

/**
 * 签名处理过滤器
 * Class FilterSign
 * @package Dhcc\RpcClient\filter
 */
class FilterSign implements Filter
{

    protected $appKey    = '';
    protected $appSecret = '';

    /**
     * FilterSign constructor.
     * @param $appKey
     * @param $appSecret
     */
    public function __construct($appKey, $appSecret)
    {
        $this->appKey    = $appKey;
        $this->appSecret = $appSecret;
    }

    public function inputFilter($data, stdClass $context)
    {
        return $data;
    }

    public function outputFilter($data, stdClass $context)
    {
        if (is_object($context->client)) {
            /**
             * @var $client Client
             */
            $client = $context->client;
            $client->setHeader('user-agent', 'rpc-client ' . RpcClient::VERSION_CLIENT);
            $client->setHeader('appkey', $this->appKey);
            $time = time();
            $client->setHeader('time', $time);
            $rand = mt_rand(100000, 999999);
            $client->setHeader('rand', $rand);
            $uriArray  = parse_url($client->uri);
            $signArray = $this->makeSign($this->appKey, $this->appSecret, $uriArray['path'], $time, $rand, $data);
            $signdebug = $client->useData['signdebug'] ?? false;
            if ($signdebug) {
                $client->setHeader('signdebug', $signdebug);
                $client->setHeader('signclient', $signArray['sign_str']);
            }
            $client->setHeader('sign', $signArray['sign']);
        }
        return $data;
    }


    public function makeSign($appkey, $appSecret, $path, $time, $rand, $dataStr)
    {
        $signStr = $appkey . '-' . $appSecret . '-' . $path . '-' . $time . '-' . $rand . '-' . $dataStr;
        return [
            'sign_str' => $signStr,
            'sign'     => md5($signStr),
        ];
    }


}